#!/usr/bin/env bash
python3 facenet_master/src/classifier.py CLASSIFY \
data/lfw_mtcnnpy_160 \
20180402-114759/20180402-114759.pb \
 models/lfw_classifier.pkl \
 --batch_size 1000 \
 --min_nrof_images_per_class 50 \
 --nrof_train_images_per_class 40 \
 --use_split_dataset
