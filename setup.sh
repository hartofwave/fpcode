#!/usr/bin/env bash
DIR="data/lfw/"
DIR2="20180402-114759/"
DIR3="venv/lib/python3.*/site-packages/"
DIR4="facenet_master"

if [ -d "venv/" ]; then
    echo "Attaching to the venv"
    source venv/bin/activate
    echo "Installing requirements.txt"
    pip3 install -q -r requirements.txt
else
    echo "No such $DIR3, is there a venv?"
    echo "Trying to make venv"
    python3 -m venv venv/
fi
#LFW data set download and check
if [ -d "$DIR" ]; then
    echo "Dir $DIR found, data set assumed to be present."
else
    if [ -d "downloads/lfw.tgz" ]; then
        mkdir data/lfw/
        tar xvf downloads/lfw.tgz -C data/
    else
        echo "No lfw.tgz, downloading LFW data set"
        wget -P downloads/ http://vis-www.cs.umass.edu/lfw/lfw.tgz
        mkdir data/lfw/
        tar xvf downloads/lfw.tgz -C data/
    fi
fi
#MODEL download and check
if [ -d "$DIR2" ]; then
    echo "Dir $DIR2 found, model assumed to be present inside it."
else
    echo "$DIR2 is Empty, downloading pre-trained model"
    wget --no-check-certificate -r 'https://drive.google.com/uc?export=download&confirm=S3qm&id=1EXPBSXwTaqrSC0OhUdXNmKSh9qJUQ55-' -O model.zip
    unzip model.zip
fi
if [ -d "$DIR4" ]; then
    echo "Dir $DIR4 found, data set assumed to be present."
else
    echo "$DIR4 is Empty, downloading facenet to facenet_master"
git clone https://github.com/HartOfWave/facenet.git facenet_master
fi
