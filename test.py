from time import strftime
import numpy as np
import matplotlib.pyplot as plt

import cv2 as cv2
import os

import predict_np_arr as pnp
import compare_np_arr as cnp

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'# removes tensorlflow debug info
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)# removes tensorlflow warnings


model = '20180402-114759/20180402-114759.pb'
classifier_filename = 'classifiers/lfw_classifier.pkl'

def main():
    for gen in range(1):
        print('Disguise search starting...')

        path = "draw/disguised/" + strftime("%Y%m%d%H%M")

        image_file = 'draw/test/Tony_Blair_0121.png'
        epochs = 5
        image = cv2.imread(image_file)
        total_image_area = image[0].size*image[1].size

        image_np_arr = np.asarray([image])  # array reshape
        base_name, base_acc = pnp.main(np.copy(image_np_arr), model, classifier_filename)
        print('Base Name: '+str(base_name) + ", Base Acc: " + str(base_acc))

        #If start shape is bad retry
        for e in range(5):
            pts = start_shape(image.shape[0])
            start_area = poly_area(pts)
            start_name, start_acc = pnp.main(np.copy(image_np_arr), model, classifier_filename)
            if start_name != -1 and start_area < 2000:
                break
            else:
                print('Start shape is bad: RESTART '+str(e+1))

        print('Start Name: '+str(start_name) + ", Start Acc: " + str(start_acc) + ", Area: " + str(start_area))
        fig, line1, line2 = graph(base_acc, start_area, epochs)

        log = np.asarray([[start_acc], [start_area], [pts], [start_name]])

        o_acc, o_area, o_pts, o_name = start_acc, start_area, pts, start_name

        for i in range(epochs):

            #print(log[2])
            n_res = search(image, log[2][i])
            n_acc, n_pts, n_area, n_name = n_res[3], n_res[2], n_res[1], n_res[0]

            if n_name == -1: # face lost keep old values
                print_search_status(i, epochs, o_acc, o_area, o_name)
            elif n_name != base_name:
                print('SUCCESS, Misclassification of '+str(base_name)+' as '+str(n_name))
                break

            elif n_area < o_area and n_acc <= o_acc: #reduce area with same accuracy lower accuracy
                print_search_status(i, epochs, n_acc, n_area, n_name)
                o_acc, o_area, o_pts, o_name = n_acc, n_area, n_pts, n_name

            elif n_acc < o_acc:  #reduce accuracy, with increased area
                print_search_status(i, epochs, n_acc, n_area, n_name)
                o_acc, o_area, o_pts, o_name = n_acc, n_area, n_pts, n_name

            else:  #no improvement, keep old values
                print_search_status(i, epochs, o_acc, o_area, o_name)

            #print(log.shape)
            log = np.concatenate((log, [[o_acc], [o_area], [o_pts], [o_name]]), axis=1)
            #live_plot(log, total_image_area, path, image, pts)
            new_plot(fig, line1, line2, log, total_image_area)
        #if o_acc < 0.6:
            #live_plot(fig, line1, line2, o_acc, o_area)
def new_plot(fig, line1, line2, log, T_area):
    line1.set_data(list(range(0, log[0].size)), log[0])
    line2.set_data(list(range(0, log[0].size)), np.multiply(log[1], 32/T_area))
    fig.canvas.draw()
    fig.canvas.flush_events()

def plot(log, T_area, path, image, pts):
    end_face = cv2.fillConvexPoly(image, pts, colour())

    cv2.imwrite(path + '/image.png', end_face)

    plt.plot(list(range(0, log[0].size)), log[0], label='Accuracy')
    plt.plot(list(range(0, log[0].size)), np.multiply(log[1], 1/T_area), label='Area')
    plt.ylim(0,1)
    plt.legend()
    plt.pause(0.05)
    os.makedirs(path, mode=0o777, exist_ok=False)
    plt.savefig(path+'/plot.png')
    plt.show()


def live_plot(log, T_area, path, image, pts):
    #plt.close()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(list(range(0, log[0].size)), log[0], label='Accuracy')
    ax.plot(list(range(0, log[0].size)), np.multiply(log[1], 1 / T_area), label='Area')
    plt.legend()
    plt.show()
    plt.pause(0.001)
    ax.clear()
def graph(base_acc, area, epochs):
    plt.ion()

    fig = plt.figure()
    ax = fig.add_subplot(111)
    line1, = ax.plot(1, base_acc, label='Accuracy') # Returns a tuple of line objects, thus the comma
    line2, = ax.plot(1, 0, label='Area')
    plt.legend()
    plt.ylim(0,1)
    plt.xlim(0,epochs)



    return fig, line1, line2

def print_search_status(n_epoch, total_epochs, acc, area, class_name):
    print("Epoch: " + str(n_epoch) + " of " +str(total_epochs)+ "; "+ class_name + ", Acc = " + str(acc) + ", Area = " + str(area))


def start_shape(x):
    #make a random start shape with three points
    pts = np.multiply(np.random.rand(3, 2), x).astype(np.int32)
    return pts


def colour():
    #colour modifications not implemented
    rgb = (0, 0, 0)
    return rgb


def search(image, pts, search_mode='local_random', evaluation_mode='acc'):


    if search_mode == 'local_random':
        search_pts = random_search(pts)
    elif search_mode == 'random_search_n':
        search_pts = random_search_n(pts, 3) #broken

    search_arr = poly_area(search_pts) # calculate shape area
    disguised_image = cv2.fillConvexPoly(np.copy(image), pts, colour())

    if evaluation_mode == 'acc':
        image_np_arr = np.asarray([disguised_image]) #array reshape
        search_acc, search_name = pnp.main(image_np_arr, model, classifier_filename) #calculate new search outputs
        out = [search_acc, search_arr, search_pts, search_name]
        return out
    elif evaluation_mode == 'e_dist':
        e_dist = draw(np.copy(image), search_pts, colour()) #calculate new search outputs
        out = [e_dist, search_arr, search_pts, 'N/A']
        return out
    else:
        print('No evaluation mode set')
        return None


def random_search(pts):
    search_range = 20
    index = [np.random.randint(0, pts.shape[0]), np.random.randint(0, 1)]
    new_xy = pts[index] + np.random.randint(-search_range, search_range)
    pts[index] = new_xy
    return pts

def random_search_n(pts, n):
    search_range = 20
    pts_arr = []
    for i in range(n):
        index = [np.random.randint(0, pts.shape[0]), np.random.randint(0, 1)]
        new_xy = pts[index] + np.random.randint(-search_range, search_range)
        pts[index] = new_xy
        search_arr = poly_area(pts)

        pts_arr = np.vstack([pts_arr, search_arr])
    return pts_arr


# https://stackoverflow.com/questions/24467972/calculate-area-of-polygon-given-x-y-coordinates#30408825
def poly_area(arr):  # calculates the area of the drawn poly
    arr = np.reshape(arr, (2, len(arr)))
    x = arr[0]
    y = arr[1]
    return 0.5*np.abs(np.dot(x, np.roll(y, 1))-np.dot(y, np.roll(x, 1)))


def draw(img, pts, colour):
    cv2.fillConvexPoly(img, pts, colour)
    class_names, probabilities = calc_acc(img)
    return probabilities, class_names


def get_image(source_dir):
    for file in os.listdir(source_dir):
        filename = os.fsdecode(file)
        if filename.endswith(".png"):
            return cv2.imread(source_dir + filename)
            continue
        else:
            return None
            continue
def calc_e_dist(img):
    #needs reimplementation
    cnp.main(img, model)
    return

def calc_acc(img):
    image_np_arr = np.asarray([img])
    return pnp.main(image_np_arr, model, classifier_filename)


if __name__ == '__main__':
    main()
