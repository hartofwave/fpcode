
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# ----------------------------------------------------
# MIT License
#
# Copyright (c) 2017 Rishi Rai
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ----------------------------------------------------

import tensorflow as tf
import numpy as np
import facenet_master.src.facenet as facenet
import facenet_master.src.align as align
import facenet_master.src.align.detect_face as detect_face
import os, sys
import pickle
from scipy import misc
from six.moves import xrange

image_size = 160
seed = 666
margin = 44
gpu_memory_fraction = 1.0

def main(image_np_arr, model, classifier_filename):

    images, cout_per_image, nrof_samples = load_and_align_data(image_np_arr, image_size, margin, gpu_memory_fraction)

    if np.any(images == -1):
        return -1, -1
    else:

        with tf.Graph().as_default():

            with tf.Session() as sess:

                # Load the model
                sys.stdout = open(os.devnull, 'w') # suppresses prints
                facenet.load_model(model)
                sys.stdout = sys.__stdout__ # enables prints

                # Get input and output tensors
                images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
                embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
                phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")

                # Run forward pass to calculate embeddings
                feed_dict = {images_placeholder: images, phase_train_placeholder: False}
                emb = sess.run(embeddings, feed_dict=feed_dict)
                # Open classifier
                classifier_filename_exp = os.path.expanduser(classifier_filename)
                with open(classifier_filename_exp, 'rb') as infile:
                    (model, class_names) = pickle.load(infile)

                predictions = model.predict_proba(emb)
                best_class_indices = np.argmax(predictions, axis=1)

                best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]

                k = 0
                for i in range(nrof_samples):
                    for j in range(cout_per_image[i]):
                        #print('%s: %.3f' % (class_names[best_class_indices[k]], best_class_probabilities[k]))
                        return class_names[best_class_indices[k]], best_class_probabilities[k]

                        k += 1


def load_and_align_data(image_np_arr, image_size, margin, gpu_memory_fraction):
    minsize = 20  # minimum size of face
    threshold = [0.6, 0.7, 0.7]  # three steps's threshold
    factor = 0.709  # scale factor

   # print('Creating networks and loading parameters')
    with tf.Graph().as_default():
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_memory_fraction)
        sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
        with sess.as_default():
            pnet, rnet, onet = align.detect_face.create_mtcnn(sess, None)

    nrof_samples = len(image_np_arr)
    img_list = []
    count_per_image = []
    for i in xrange(nrof_samples):
        img = image_np_arr[i]
        img_size = np.asarray(img.shape)[0:2]
        bounding_boxes, _ = align.detect_face.detect_face(img, minsize, pnet, rnet, onet, threshold, factor)
        count_per_image.append(len(bounding_boxes))
        for j in range(len(bounding_boxes)):
            det = np.squeeze(bounding_boxes[j, 0:4])
            bb = np.zeros(4, dtype=np.int32)
            bb[0] = np.maximum(det[0] - margin / 2, 0)
            bb[1] = np.maximum(det[1] - margin / 2, 0)
            bb[2] = np.minimum(det[2] + margin / 2, img_size[1])
            bb[3] = np.minimum(det[3] + margin / 2, img_size[0])
            cropped = img[bb[1]:bb[3], bb[0]:bb[2], :]
            aligned = misc.imresize(cropped, (image_size, image_size), interp='bilinear')
            prewhitened = facenet.prewhiten(aligned)
            img_list.append(prewhitened)
    try:
        images = np.stack(img_list)
    except ValueError:
        images = np.asarray([-1])
        print('Face Lost') # Face no longer detectable error

    return images, count_per_image, nrof_samples
