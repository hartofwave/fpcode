# https://jekel.me/2017/How-to-detect-faces-using-facenet/

#   import facenet libraires
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import cv2
import matplotlib.pyplot as plt
import tensorflow as tf
import facenet.src.align as align
import facenet.src.align.detect_face


#   setup facenet parameters
from numpy.doc import misc

gpu_memory_fraction = 1.0
minsize = 50  # minimum size of face
threshold = [0.6, 0.7, 0.7]  # three steps's threshold
factor = 0.709  # scale factor

#   fetch images
#image_dir = 'test_mod/robertk/'

#   create a list of your images
#images = os.listdir(image_dir)


def isface(img):
    #   Start code from facenet/src/compare.py
#    print('Creating networks and loading parameters')
    with tf.Graph().as_default():
        gpu_options = tf.GPUOptions(
            per_process_gpu_memory_fraction=gpu_memory_fraction)
        sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options,
            log_device_placement=False))
        with sess.as_default():
            pnet, rnet, onet = align.detect_face(
                sess, None)

            bounding_boxes, _ = align.detect_face.detect_face(
                    img, minsize, pnet,
                    rnet, onet, threshold, factor)

            for (x1, y1, x2, y2, acc) in bounding_boxes:
                w = x2 - x1
                h = y2 - y1
                #   plot the box using cv2
                cv2.rectangle(img, (int(x1), int(y1)), (int(x1 + w),
                                                        int(y1 + h)), (255, 0, 0), 2)
                print('Accuracy score', acc)