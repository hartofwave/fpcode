#!/usr/bin/env bash
python3 facenet/src/align/align_dataset_mtcnn.py \
data/lfw/ \
data/lfw-align/ \
--image_size 182 \
--margin 44 \
--random_order \
--gpu_memory_fraction 0.25
